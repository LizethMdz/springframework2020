package mx.com.lamt.users.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(getInfo())
			.select().apis(RequestHandlerSelectors.basePackage("mx.com.lamt.users.controllers"))
			//.select().apis(RequestHandlerSelectors.any())
			// Si deseamos especificar solo ciertos controller, se declara asi
			// .paths(Predicates.or(PathSelectors.ant("/roles/*"),PathSelectors.ant("/users/*")))
			.paths(PathSelectors.any()).build()
			// Para deshabilitar los staus code por default
			.useDefaultResponseMessages(false);
	}

	private ApiInfo getInfo() {
		// TODO Auto-generated method stub
		return new ApiInfoBuilder()
				.title("LAMT API REST")
				.version("1.0")
				.license("Apache 2.0")
				.contact(new Contact("@lamt", "http://www.lamt.com", "contacto@lamt.com"))
				.build();
	}
}
