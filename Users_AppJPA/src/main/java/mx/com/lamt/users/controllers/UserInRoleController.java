package mx.com.lamt.users.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.lamt.users.entities.UserInRole;
import mx.com.lamt.users.services.UserInRoleService;

@RestController
@RequestMapping("/users/{userId}/role/{roleId}/userInRole")
public class UserInRoleController {
	
	@Autowired
	private UserInRoleService service;
	
	@GetMapping
	public ResponseEntity<List<UserInRole>> getRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId){
		return new ResponseEntity<List<UserInRole>>(service.getRoleByUserId(userId, roleId), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<UserInRole> create(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId, @RequestBody UserInRole userInRole){
		return new ResponseEntity<UserInRole>(service.createRoleToUser(userId, roleId, userInRole), HttpStatus.CREATED);
	}
}
