package mx.com.lamt.users.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import mx.com.lamt.users.entities.Role;
import mx.com.lamt.users.entities.User;
import mx.com.lamt.users.entities.UserInRole;
import mx.com.lamt.users.repositories.RoleRepository;
import mx.com.lamt.users.repositories.UserInRoleRepository;
import mx.com.lamt.users.repositories.UserRepository;

@Service
public class UserInRoleService {
	@Autowired 
	private UserInRoleRepository userInRoleRepository;
	
	@Autowired 
	private UserRepository userRepository;
	
	@Autowired 
	private RoleRepository roleRepository;

	public UserInRole createRoleToUser(Integer userId, Integer roleId, UserInRole userInRole) {
		// TODO Auto-generated method stub
		Optional<User> result = userRepository.findById(userId);
		Optional<Role> result2 = roleRepository.findById(roleId);
		if(result.isPresent() && result2.isPresent()) {
			userInRole.setUser(result.get());
			userInRole.setRole(result2.get());
			return userInRoleRepository.save(userInRole);
		}else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Neither User %d and Role %d found", userId, roleId));
		}
		
	}

	public List<UserInRole> getRoleByUserId(Integer userId, Integer roleId) {
		// TODO Auto-generated method stub
		return userInRoleRepository.findByUserIdAndRoleId(userId, roleId);
	}
	
	public List<UserInRole> getRolesByUserId(Integer userId){
		return userInRoleRepository.findRolesByUserId(userId);
	}
}
