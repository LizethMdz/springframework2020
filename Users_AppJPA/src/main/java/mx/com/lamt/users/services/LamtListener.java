package mx.com.lamt.users.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class LamtListener {
	private static final Logger log = LoggerFactory.getLogger(LamtListener.class);

	@KafkaListener(topics = "lamt-topic", groupId = "lamtGroup")
	public void listen(String message) {
		log.info("Message received este es el mensaje en tiempo real {}", message);
		try {
			Thread.sleep(5000l);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
