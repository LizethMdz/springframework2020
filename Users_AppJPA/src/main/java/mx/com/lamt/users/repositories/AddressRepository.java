package mx.com.lamt.users.repositories;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.com.lamt.users.entities.*;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{

	@Query("SELECT a FROM Address a where a.profile.user.id=?1 AND a.profile.id=?2")
	List<Address> findByProfileId(Integer userId, Integer profileId);

}
