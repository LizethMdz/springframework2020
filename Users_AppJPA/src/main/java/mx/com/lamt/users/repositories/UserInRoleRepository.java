package mx.com.lamt.users.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.com.lamt.users.entities.User;
import mx.com.lamt.users.entities.UserInRole;

public interface UserInRoleRepository extends JpaRepository<UserInRole, Integer>  {
	
	@Query("SELECT u FROM UserInRole u WHERE u.user.id=?1 AND u.role.id=?2")
	public List<UserInRole> findByUserIdAndRoleId(Integer userId, Integer roleId);
	
	@Query("SELECT u FROM UserInRole u WHERE u.user.id=?1")
	public List<UserInRole> findRolesByUserId(Integer userId);
	
	@Query("SELECT u.user FROM UserInRole u WHERE u.role.name=?1")
	public List<User> findUserByRole(String roleName);
	
	public List<UserInRole> findByUser(User user);
}
