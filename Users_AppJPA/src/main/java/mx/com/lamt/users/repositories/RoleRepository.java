package mx.com.lamt.users.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.lamt.users.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
