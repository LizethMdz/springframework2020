package mx.com.lamt.users.services;

import java.util.List;
import java.util.Optional;

//import javax.annotation.security.RolesAllowed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.access.prepost.PostAuthorize;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.lamt.users.entities.Role;
import mx.com.lamt.users.entities.User;
import mx.com.lamt.users.models.AuditDetails;
import mx.com.lamt.users.models.LamtSecurityRule;
import mx.com.lamt.users.repositories.RoleRepository;
import mx.com.lamt.users.repositories.UserInRoleRepository;

@Service
@LamtSecurityRule
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserInRoleRepository userInRoleRepository;
	
	// Define la llave y el mensaje
	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;
	
	// Para poder crear un objeto JSON
	private ObjectMapper mapper = new ObjectMapper();
	
	private static final Logger log = LoggerFactory.getLogger(RoleService.class);
	
	
	public List<Role> getRoles(){
		return roleRepository.findAll();
	}
	
	// @Secured({"ROLE_ADMIN","ROLE_USER"})
	// @RolesAllowed({"ROLE_ADMIN"})
	// Pueden invocar la peticion el método
	/*@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')") */ 
	// Se removieron, pero se creo una meta anotacion.
	// Solo el admin puede obtener una respuesta
	//@PostAuthorize("hasRole('ROLE_ADMIN')")
	/*Nueva meta - anotación personalizada, se puso a nivel de clase*/
	
	public List<User> getUsersByRole(String roleName){
		log.info("Getting roles by name {}", roleName);
		return userInRoleRepository.findUserByRole(roleName);
	}
	
	/*public Role createRole(Role role) {
		Role roleCreated = roleRepository.save(role);
		return roleCreated;
	}*/
	
	public Role createRole(Role role) {
		Role roleCreated = roleRepository.save(role);

		AuditDetails details = new AuditDetails(SecurityContextHolder.getContext().getAuthentication().getName(),
				role.getName());
		try {
			kafkaTemplate.send("devs4j-topic", mapper.writeValueAsString(details));
		} catch (JsonProcessingException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error parsing the message ");
		}

		return roleCreated;
	}
	
	public Role updateRole(Integer roleId, Role role) {
		Optional<Role> result = roleRepository.findById(roleId);
		if (result.isPresent()) {
			return roleRepository.save(role);
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Role id %d doesn't exists", roleId));
		}
	}
	
	public void deleteRole(Integer roleId) {
		Optional<Role> result = roleRepository.findById(roleId);
		if (result.isPresent()) {
			roleRepository.delete(result.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Role id %d doesn't exists", roleId));
		}
	}
}
