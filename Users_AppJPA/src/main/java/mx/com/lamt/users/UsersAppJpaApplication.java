package mx.com.lamt.users;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.github.javafaker.Faker;

import mx.com.lamt.users.entities.Role;
import mx.com.lamt.users.entities.User;
import mx.com.lamt.users.entities.UserInRole;
import mx.com.lamt.users.repositories.RoleRepository;
import mx.com.lamt.users.repositories.UserInRoleRepository;
import mx.com.lamt.users.repositories.UserRepository;

@SpringBootApplication
public class UsersAppJpaApplication implements ApplicationRunner{
	
	@Autowired
	private Faker faker;
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserInRoleRepository userInRoleRepository;

	private static final Logger log = LoggerFactory.getLogger(UsersAppJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(UsersAppJpaApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Role roles[] = { new Role("ADMIN"), new Role("SUPPORT"), new Role("USER") };

		for (Role role : roles) {
			roleRepository.save(role);
		}
		
		// TODO Auto-generated method stub
		for (int i = 0; i <= 10; i++) {
			User user = new User();
			user.setUsername(faker.name().username());
			user.setPassword(faker.dragonBall().character());
			//user.setProfile(null);
			User created = repository.save(user);
			UserInRole userInRole = new UserInRole(created, roles[new Random().nextInt(3)]);
			log.info("USer created username {} password {} role {}", created.getUsername(), created.getPassword(),
					userInRole.getRole().getName());
			userInRoleRepository.save(userInRole);
		}
	}

}
