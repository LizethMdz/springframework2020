package mx.com.lamt.users.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //Stereotype
@RequestMapping("/api")
public class LamtController {
	
	@GetMapping("/")
	public String helloWorld() {
		return "Hello World";
	}
}
