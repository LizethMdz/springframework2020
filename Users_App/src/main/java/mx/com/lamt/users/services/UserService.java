package mx.com.lamt.users.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.github.javafaker.Faker;

import mx.com.lamt.users.models.User;

@Service
public class UserService {
	@Autowired
	private Faker faker;
	
	/*Codigo de mi servicio*/
	private List<User> users = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		for (int i = 0; i < 20; i++) {
			users.add(new User(faker.funnyName().name(), faker.name().username(), faker.dragonBall().character()));			
		}
	}
	
	public List<User> getUsers(){
		return users;
	}
	
	public List<User> getUsersWithQueryParams(String startWith){
		if(startWith != null) {
			return users.stream().filter(u -> u.getUsername().startsWith(startWith)).collect(Collectors.toList());
		}else {
			return users;
		}
	}
	 
	public User getUserByUsername(String username) {
		return users.stream().filter(u -> u.getUsername().equals(username))
				.findAny()
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %s not found", username)));
	}
	
	public User createUser(User user) {
		if(users.stream().anyMatch(u -> u.getUsername().equals(user.getUsername()))) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("User %s already exist", user.getUsername()));
		}
		users.add(user);
		return user;
	}
	
	public User updateUser(User user, String username) {
		User userToBeUpdated = getUserByUsername(username);
		userToBeUpdated.setNickName(user.getNickName());
		userToBeUpdated.setPassword(user.getPassword());

		return userToBeUpdated;
	}
	
	
	public void deleteUser(String username) {
		User userToBeRemove = getUserByUsername(username);
		users.remove(userToBeRemove);
	}
	
	
}
