package mx.com.lamt.di.lifecycle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy // @Lazy - Si el bean es lazy no se inicializará a menos que se inyecte // Descomentar para ver su funcionamiento
// @Scope("prototype") -- No se ejecutará el callback predestroy si este scope es prototype
public class LifeCycleBean implements  BeanNameAware, InitializingBean, DisposableBean {

	
	private static final Logger log = LoggerFactory.getLogger(LifeCycleBean.class);

	/**
	 * Se ejecuta durante la construcción del bean "BeanNameAware"
	 * */
	@Override
	public void setBeanName(String name) {
		// TODO Auto-generated method stub
		log.info("Bean name {}", name);
	}
	/*Uso de Callbacks*/
	// No deben de recibir parametros
	/**
	 * Se ejecutarán después de la inyección de dependencias
	 * */
	@PostConstruct
	public void init() {
		log.info("Post Construct");
	}
	
	
	/**
	 * Se ejecutarán antes de que el bean sea desctrido
	 * No se ejecutan para beans prototype
	 * Solo se ejecutan durante una terminación de la VM de formal normal.
	 * */
	@PreDestroy
	public void destroyBean() {
		log.info("Pre Destroy");
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		log.info("After properties set method");
		
	}
	
	@Override
	public void destroy() throws Exception {
		log.info("Destoy method");
	}

}
