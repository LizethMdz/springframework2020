package mx.com.lamt.di.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Circle implements Figure {
	
	// Accediendo a las propiedades de un .properties
	@Value("${circle.radius:2.5}")
	//@Value("2.5")
	public double radio;
	
	@Override
	public double calcularArea() {
		// TODO Auto-generated method stub
		return Math.PI*Math.pow(radio, 2);
	}

}
