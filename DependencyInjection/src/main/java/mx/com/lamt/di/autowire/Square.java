package mx.com.lamt.di.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Square implements Figure {
	
	@Value("5.0")
	public double side;

	@Override
	public double calcularArea() {
		// TODO Auto-generated method stub
		return Math.pow(side, 2);
	}

}
