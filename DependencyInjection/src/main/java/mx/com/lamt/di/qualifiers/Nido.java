package mx.com.lamt.di.qualifiers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
public class Nido {
	
	@Autowired
	//Para que el contexto sepa que inyectar como
	// implementacion
	//@Qualifier("perrito") 
	private Animal animal;
	
	
	private static final Logger log = LoggerFactory.getLogger(Nido.class);

	
	public void imprimir() {
		log.info("Mi nido tiene un animal = {}", animal.getNombre());
	}
}
