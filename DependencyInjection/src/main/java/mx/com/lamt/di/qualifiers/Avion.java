package mx.com.lamt.di.qualifiers;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Avion implements Volador {

	private static final Logger log = org.slf4j.LoggerFactory.getLogger(Pajaro.class);
	@Override
	public void volar() {
		// TODO Auto-generated method stub
		log.info("Soy una avion estoy volando");
	}

}
