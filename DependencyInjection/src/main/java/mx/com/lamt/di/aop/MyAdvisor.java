package mx.com.lamt.di.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1) //Define el orden en el que se van a ejecutar los aspectos
public class MyAdvisor {
	
	
	private static final Logger log = LoggerFactory.getLogger(MyAdvisor.class);
	
	// Ejecución de métodos con @Before (AOP)
	// "valor de retorno paquete bean .metodo (parametros)"
	// Se puede declarar así o bien se emplean un Pointcut
	//@Before("execution(* mx.com.lamt.di.aop.TargetObject.*(..))")
	@Before("PointcutExample.targetObjectMethods()")
	public void before(JoinPoint joinPoint) {
		log.info("1.Advisor - Method name: {}", joinPoint.getSignature().getName());
		log.info("1.Advisor - Object type: {}", joinPoint.getSignature().getDeclaringTypeName());
		log.info("1.Advisor - Modifiers {}", joinPoint.getSignature().getModifiers());
		log.info("1.Advisor - Arguments {}", joinPoint.getArgs());
		log.info("1.Advisor - Before advise");
	}
}
