package mx.com.lamt.di.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import mx.com.lamt.di.DependencyInjectionApplication;

@Component
public class TargetObject {
	
	
	private static final Logger log = LoggerFactory.getLogger(TargetObject.class);
    
	@LamtAnnotation
	public void hello(String message) {
		log.info(message);
	}
	
	public void foo() {
		log.info("Foo");
	}
}
