package mx.com.lamt.di.qualifiers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
//Si nuestro Bean lo definimos como por default
//Agregamos la anotation @Primary
@Primary
public class Pajaro extends Animal implements Volador{
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(Pajaro.class);

	public Pajaro(@Value("Merlin") String nombre,  @Value("6")Integer edad) {
		super(nombre, edad);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public void volar() {
		// TODO Auto-generated method stub
		log.info("Soy un pajaro Estoy volando");
	}

}
