package mx.com.lamt.di;

import java.beans.Expression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import mx.com.lamt.di.aop.TargetObject;
import mx.com.lamt.di.atributo.Coche;
import mx.com.lamt.di.atributo.Motor;
import mx.com.lamt.di.autowire.AreaCalculatorService;
import mx.com.lamt.di.lifecycle.ExplicitBean;
import mx.com.lamt.di.lifecycle.LifeCycleBean;
import mx.com.lamt.di.profiles.EnviromentService;
import mx.com.lamt.di.qualifiers.Animal;
import mx.com.lamt.di.qualifiers.Nido;
import mx.com.lamt.di.qualifiers.Pajaro;
import mx.com.lamt.di.qualifiers.Perro;
import mx.com.lamt.di.scopes.EjemploScopeService;

@SpringBootApplication
public class DependencyInjectionApplication {

	@Bean
	public String getApplicationName() {
		return "Lamt rules";
	}
	
	/*Decalración explicita de un Bean*/
	/*@Bean(initMethod = "init", destroyMethod = "destroy")
	public ExplicitBean getBean() {
		return new ExplicitBean();
	}*/
	
	private static final Logger log = LoggerFactory.getLogger(DependencyInjectionApplication.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(DependencyInjectionApplication.class, args);
		/*@Qualifier and @Autowired*/
		//Animal animal = context.getBean("perrito", Animal.class);
		//log.info("Objeto nombre =  {}, edad = {}", animal.getNombre(), animal.getNombre());
		
		/*@Primary*/
		/*Nido nido = context.getBean(Nido.class);
		nido.imprimir();*/
		
		/*Profiles*/
		/*EnviromentService enviromentService = context.getBean(EnviromentService.class);
	    log.info("Active environment {}", enviromentService.getEnviroment());*/
		
		/*Scopes*/
		/*EjemploScopeService ess = context.getBean(EjemploScopeService.class);
		EjemploScopeService ess1 = context.getBean(EjemploScopeService.class);
		log.info("Are beans equal {}", ess.equals(ess1));
		log.info("Are beans == {}", ess == ess1);*/
		
		/*Declaracion explicita de Beans*/
		/*String nombreAplicacion = context.getBean(String.class);
		log.info("Nombre de la aplicacion {}", nombreAplicacion);*/
		
		/*Autowired para el uso de List<> y Objetos*/
		/*AreaCalculatorService acs = context.getBean(AreaCalculatorService.class);
		log.info("Area Total: {}", acs.calcAreas());*/
		
		/*Spring Expression Language*/ 
		/*Sin el uso de Spring*/
		/*ExpressionParser parser = new SpelExpressionParser();
		// Expression expression = parser.parseExpression("10 + 20 "); Doesn't work
		parser.parseExpression("10 + 20 ");
		log.info("Result {}", parser.parseExpression("10 + 20 ").getValue());*/

		/*Life Cycle Bean*/
		//context.getBean(LifeCycleBean.class);
		
		//Creación de un aspecto
		TargetObject targetObject = context.getBean(TargetObject.class);
		targetObject.hello("Hello World");
		targetObject.foo();
	}
}
	
