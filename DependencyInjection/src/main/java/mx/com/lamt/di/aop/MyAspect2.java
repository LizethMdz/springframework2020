package mx.com.lamt.di.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(0)
public class MyAspect2 {
	
	private static final Logger log = LoggerFactory.getLogger(MyAspect2.class);
	
	@Before("execution(* mx.com.lamt.di.aop.TargetObject.*(..))")
	public void before(JoinPoint joinPoint) {
		log.info("2.Advisor - Method name: {}", joinPoint.getSignature().getName());
		log.info("2.Advisor - Object type: {}", joinPoint.getSignature().getDeclaringTypeName());
		log.info("2.Advisor - Modifiers {}", joinPoint.getSignature().getModifiers());
		log.info("2.Advisor - Arguments {}", joinPoint.getArgs());
		log.info("2.Advisor - Before advise");
	}

}
