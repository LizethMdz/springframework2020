package mx.com.lamt.di.autowire;

public interface Figure {
	double calcularArea();
}
