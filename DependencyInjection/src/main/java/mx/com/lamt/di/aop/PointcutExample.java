package mx.com.lamt.di.aop;

import org.aspectj.lang.annotation.Pointcut;

public class PointcutExample {
	
	//@Pointcut("execution(* mx.com.lamt.di.aop.TargetObject.*(..))")
	/*Existe otra forma de declarar los aspectos para los que pertenezcan a un paquete*/
	//@Pointcut("within(mx.com.lamt.di.aop.*)")
	/*Si solo especificamos la Clase a Interceptar con Aspectos*/
	//@Pointcut("TargetObject)")
	/*Si trabajamos com anotaciones personalizadas*/
	@Pointcut("@annotation(LamtAnnotation)")
	public void targetObjectMethods() {
		
	}
}
