package mx.com.lamt.di.atributo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Motor {
	//@Value("XL1") A nivel de atributo
	private String marca;
	//@Value("1982")
	private Integer modelo;
	
	

	public Motor() {
	}

	// A nivel de constructor
	// public Motor(@Value("XL1") String marca, @Value("1982")Integer modelo)
	public Motor(String marca, Integer modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	//A nivel de setters
	@Value("XL1")
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getModelo() {
		return modelo;
	}

	@Value("1982")
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	} 

	@Override
	public String toString() {
		return "Motor [marca=" + marca + ", modelo=" + modelo + "]";
	}
	
	
	
}
